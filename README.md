# Phoenix

Phoenix is a web application that uses React server-side rendering with Spring Boot and GraalVM to execute JavaScript.

## Project Structure

The project is organized into the following directories:

- `backend`: This directory contains the Spring Boot backend code.
- `frontend`: This directory contains the React frontend code.
- `docker`: This directory contains Docker configuration files.
- `gradle`: This directory contains Gradle build files.

### Frontend

The frontend code is located in the `src` directory and configuration files for ESLint, Prettier, and Stylelint can be found in the root of the `frontend` directory.

### Backend

The backend code is located in the `src` directory. Docker configuration for the backend is located in the `dockerfile`.

## Prerequisites

- [Docker](https://www.docker.com/get-started)
- [Gradle](https://gradle.org/install/)
- [Node.js](https://nodejs.org/en/download/)
- [Graal VM Java 17](https://www.graalvm.org/docs/getting-started/)

## Getting Started

1. Clone the repository
```bash
git clone https://github.com/username/phoenix.git
```

2. Navigate to the project directory
```bash
cd phoenix
```

## Running the Application

- Development mode
Run the following script to start the application in development mode:
```bash
sh runDev.sh
```
This script builds and starts the docker containers defined in docker/docker-compose-dev.yaml.
- Production mode
Run the following script to start the application in production mode:
```bash
docker-compose -f docker/docker-compose.yaml up --build
 ```
This script builds and starts the docker containers defined in docker/docker-compose.yaml.
