import React from 'react';

export function ProductPage() {
    const producst = [
        {
            id: 1,
            name: 'Product 1',
            description: 'This is product 1',
            price: 100,
        },
        {
            id: 2,
            name: 'Product 2',
            description: 'This is product 2',
            price: 200,
        },
    ];

    return (
        <div>
            <h1>Products</h1>
            <ul>
                {producst.map((product) => (
                    <li key={product.id}>
                        <h3>{product.name}</h3>
                        <p>{product.description}</p>
                        <p>{product.price}</p>
                    </li>
                ))}
            </ul>
        </div>
    );
}
