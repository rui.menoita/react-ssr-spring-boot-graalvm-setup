import React from 'react';
import App from '../components/App';
import { Route, Routes } from 'react-router-dom';
import { ProductPage } from './ProductPage';

export function MyReactAppRoutes() {
    return (
        <Routes>
            <Route path="/" element={<App />} />
            <Route path={'/product'} element={<ProductPage />} />
            <Route path={'/home'} element={<h1>Home</h1>} />
            <Route path={'/about'} element={<h1>About</h1>} />
        </Routes>
    );
}
