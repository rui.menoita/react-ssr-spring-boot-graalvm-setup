import React from 'react';
import ReactDOMServer from 'react-dom/server';
import { createRoot, hydrateRoot } from 'react-dom/client';
import { StaticRouter } from 'react-router-dom/server';
import { MyReactAppRoutes } from './pages/Routes';
import { BrowserRouter } from 'react-router-dom';

const anyWindow: any = window;

anyWindow.renderApp = () => {
    const dontuUseSSR = process.env.REACT_APP_SSR === 'false';
    if (dontuUseSSR) {
        createRoot(document.getElementById('root')!).render(
            <BrowserRouter>
                <MyReactAppRoutes />
            </BrowserRouter>
        );
    } else {
        hydrateRoot(
            document.getElementById('root')!,
            <BrowserRouter>
                <MyReactAppRoutes />
            </BrowserRouter>
        );
    }
};
anyWindow.renderAppOnServer = () => {
    return ReactDOMServer.renderToString(
        <StaticRouter location={anyWindow.requestUrl}>
            <MyReactAppRoutes />
        </StaticRouter>
    );
};

anyWindow.isServer = false;
