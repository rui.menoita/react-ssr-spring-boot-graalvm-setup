package com.example.app.controller;

import com.example.app.render.GraalVMRenderer;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * This controller is responsible for rendering the HTML page using GraalVMRenderer.
 */
@Slf4j
@Controller
@RequiredArgsConstructor
public class HtmlController {

    private final GraalVMRenderer graalVMRenderer;

    /**
     * Handles GET requests to the root and any path not containing a dot and returns an HTML response.
     * The rendering is done asynchronously.
     *
     * @param request the HTTP request.
     * @return a ResponseEntity containing the rendered HTML.
     */
    @Async
    @GetMapping(value = {"/", "/{path:[^\\.]*}"}, produces = MediaType.TEXT_HTML_VALUE)
    public ResponseEntity<String> renderApp(HttpServletRequest request) {
        return ResponseEntity.ok(graalVMRenderer.render(request));
    }
}
