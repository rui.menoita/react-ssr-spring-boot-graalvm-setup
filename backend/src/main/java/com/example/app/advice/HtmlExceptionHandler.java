package com.example.app.advice;

import com.example.app.render.GraalVMRenderer;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * This class handles exceptions during HTML rendering. It uses GraalVMRenderer to render an error page.
 */
@ControllerAdvice
@RequiredArgsConstructor
public class HtmlExceptionHandler {

    private final GraalVMRenderer graalVMRenderer;

    /**
     * Handles HtmlRenderingException and returns an error page as the response.
     *
     * @param ex the exception to be handled.
     * @return a ResponseEntity containing the error page.
     */
    @ExceptionHandler({HtmlRenderingException.class})
    public ResponseEntity<String> handleHtmlRenderingException(HtmlRenderingException ex) {
        // Log error details
        return ResponseEntity.internalServerError()
                .contentType(MediaType.TEXT_HTML)
                .body(graalVMRenderer.renderErrorPage(ex));
    }

    /**
     * This class represents an exception that occurs during HTML rendering.
     */
    public static class HtmlRenderingException extends RuntimeException {

        /**
         * Constructs a new HtmlRenderingException with the specified detail message.
         *
         * @param message the detail message.
         */
        public HtmlRenderingException(String message) {
            super(message);
        }

        public HtmlRenderingException(Exception e) {
            super(e);
        }
    }
}
