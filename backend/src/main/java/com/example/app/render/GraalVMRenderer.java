package com.example.app.render;

import com.example.app.advice.HtmlExceptionHandler;
import jakarta.annotation.PreDestroy;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.graalvm.polyglot.Context;
import org.graalvm.polyglot.Source;
import org.graalvm.polyglot.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static java.lang.String.format;

/**
 * This class is responsible for rendering JavaScript files with GraalVM.
 * It uses a thread-local context for each request to ensure thread safety.
 */
@Slf4j
@Component
public class GraalVMRenderer {

    /**
     * The HTML index page as a string.
     */
    private final String index;

    /**
     * The JavaScript source to be rendered.
     */
    private final List<Source> jsSources;

    /**
     * A ThreadLocal instance to hold the GraalVM context for each thread.
     */
    private final ThreadLocal<Context> contextThreadLocal = ThreadLocal.withInitial(this::initContext);

    /**
     * A string to be replaced in the HTML with the rendered app.
     */
    private static final String REPLACE_FOR_HTML = "${SERVER_STRING}";

    /**
     * Initializes a new GraalVM context.
     *
     * @return the new context.
     */
    private Context initContext() {
        Context context = Context.create();
        //TODO add real TextEncoder and TextDecoder
        context.eval("js", "this.TextEncoder = function() { this.encode = function(s) { return s; } }");
        context.eval("js", "this.TextDecoder = function() { this.decode = function(s) { return s; } }");
        context.eval("js", "var window = {}"); // create a 'window' object
        jsSources.forEach(context::eval);
        return context;
    }

    /**
     * Creates a new instance of the GraalVMRenderer.
     *
     * @param indexResource           a reference to the HTML index page.
     * @param resourcePatternResolver a reference to the resource pattern resolver.
     */
    public GraalVMRenderer(@org.springframework.beans.factory.annotation.Value("classpath:reactApp/index.html") Resource indexResource,
                           ResourcePatternResolver resourcePatternResolver) {
        try {
            this.index = indexResource.getContentAsString(StandardCharsets.UTF_8);
            this.jsSources = Arrays.stream(resourcePatternResolver.getResources("classpath:reactApp/static/js/main.*.js"))
                    .map(this::createSource)
                    .toList();
        } catch (IOException e) {
            log.error(e.getMessage(), e);
            throw new HtmlExceptionHandler.HtmlRenderingException(e);
        }
    }

    /**
     * Creates a new source from the provided resource.
     *
     * @param resource the resource to create the source from.
     * @return the new source.
     */
    private Source createSource(Resource resource) {
        try {
            return Source.newBuilder("js", resource.getContentAsString(StandardCharsets.UTF_8), resource.getFilename()).buildLiteral();
        } catch (IOException e) {
            log.error("Error creating js source", e);
            throw new HtmlExceptionHandler.HtmlRenderingException(e);
        }
    }

    /**
     * Renders the JavaScript source and returns the resulting HTML.
     *
     * @param request the HTTP request.
     * @return the resulting HTML.
     */
    public String render(HttpServletRequest request) {
        return render(Map.of(), request);
    }

    /**
     * Renders the JavaScript source with the provided properties and returns the resulting HTML.
     *
     * @param properties the properties to use during rendering.
     * @param request    the HTTP request.
     * @return the resulting HTML.
     * @throws HtmlExceptionHandler.HtmlRenderingException if an error occurs during rendering.
     */
    public String render(Map<String, Object> properties, HttpServletRequest request) throws HtmlExceptionHandler.HtmlRenderingException {
        try {
            Context context = contextThreadLocal.get();
            context.eval("js", format("window.requestUrl = '%s'", request.getRequestURI()));
            properties.forEach((key, value) -> context.getBindings("js").putMember(key, value));
            Value renderAppOnServer = context.getBindings("js").getMember("window").getMember("renderAppOnServer");
            String html = renderAppOnServer.execute().asString();
            return addAppToIndex(html);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new HtmlExceptionHandler.HtmlRenderingException(e.getMessage());
        }
    }

    /**
     * Adds the rendered app to the index page.
     *
     * @param html the rendered app.
     * @return the complete HTML page.
     */
    private String addAppToIndex(String html) {
        StringBuilder indexHtml = new StringBuilder(index);
        int start = indexHtml.indexOf(REPLACE_FOR_HTML);
        if (start != -1) {
            indexHtml.replace(start, start + REPLACE_FOR_HTML.length(), html);
        }
        return indexHtml.toString();
    }

    /**
     * Unloads the GraalVM context from the current thread.
     */
    @PreDestroy
    public void unloadContext() {
        contextThreadLocal.get().close();
        contextThreadLocal.remove();
    }

    /**
     * Renders an error page with the message from the provided exception.
     *
     * @param e the exception to take the message from.
     * @return the HTML for the error page.
     */
    public String renderErrorPage(Exception e) {
        return "<html><body><h1>Internal Server Error</h1><p>" + e.getMessage() + "</p></body></html>";
    }
}
