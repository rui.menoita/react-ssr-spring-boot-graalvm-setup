import com.github.gradle.node.npm.task.NpmTask

plugins {
    java
    id("org.springframework.boot") version "3.1.2"
    id("io.spring.dependency-management") version "1.1.2"
    id("com.github.node-gradle.node") version "5.0.0"
}

group = "com.lumiglyph"
version = "0.0.1-SNAPSHOT"

java {
    sourceCompatibility = JavaVersion.VERSION_17
    targetCompatibility = JavaVersion.VERSION_17
}

configurations {
    compileOnly {
        extendsFrom(configurations.annotationProcessor.get())
    }
}

repositories {
    mavenLocal()
    gradlePluginPortal()
    google()
    mavenCentral()
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-actuator")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.session:spring-session-core")
    testImplementation("org.springframework.boot:spring-boot-starter-test")
    annotationProcessor("org.springframework.boot:spring-boot-configuration-processor")
    developmentOnly("org.springframework.boot:spring-boot-devtools")
    implementation("org.graalvm.js:js:23.0.0")
    implementation("org.graalvm.js:js-scriptengine:23.0.0")
    compileOnly("org.projectlombok:lombok")
    runtimeOnly("io.micrometer:micrometer-registry-prometheus")
    runtimeOnly("org.postgresql:postgresql")
    annotationProcessor("org.projectlombok:lombok")
}

// Configuration for the Gradle Node plugin
node {
    // The Node.js and npm versions to use are specified, as well as the directory of the Node.js project
    download.set(true)
    version.set("20.4.0")
    npmVersion.set("9.7.2")
    nodeProjectDir.set(file("../frontend"))
}

// Task to run "npm run build" in the Node.js project
task<NpmTask>("npmRunBuild") {
    dependsOn("npmInstall")
    args.set(listOf("run", "build"))
}

// Task to clean the directory where the built Node.js project will be copied to
task<Delete>("cleanReactApp") {
    fileTree("src/main/resources/reactApp").visit {
        delete(file) // delete each file
    }
    dependsOn("npmRunBuild")  // This task depends on the "npmRunBuild" task
}

// Task to copy the built Node.js project to a directory in the Java project
task<Copy>("copyBuild") {
    from("../frontend/build/")
    into("src/main/resources/reactApp/")
    dependsOn("cleanReactApp")// This task depends on the "cleanReactApp" task
}

tasks.bootJar  {
    mainClass.set("com.lumiglyph.PhoenixApplication")
    archiveClassifier.set("")
}

